# Class: puppet::service
#
# manage puppet services: agent and master
#
class puppet::service::agent
{

    service { 'puppet':
        ensure => 'running',
        hasstatus => true,
    }
}

class puppet::service::master
{
    if $puppet::ismaster {
        service { 'puppetserver':
            ensure => 'running',
            hasstatus => true,

            subscribe => Class['puppet::config'],
        }
    }
    
}



# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
