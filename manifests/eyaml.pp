# Class: puppet:eyaml
#
# install hiera-eyaml and set up eyaml keys
#
# Parameters:
#   $first_parameter:
#       description
#
# Actions:
#   - 
#
# Requires:
#   - 
#
# Sample Usage:
#
class puppet::eyaml (
  $pubkey = undef,
  $privkey = undef )

{

  require puppet
  
  package { ['hiera-eyaml']: provider => 'puppet_gem' }

  file { '/opt/puppetlabs/bin/eyaml':
    ensure => 'link',
    target => '/opt/puppetlabs/puppet/lib/ruby/vendor_gems/bin/eyaml',
  }
  
  
  File {
    owner => $puppet::ismaster ? {
      true => 'puppet',
      default => 'root',
    },
    group => 'admin',
  }
    
  if $privkey != undef or $pubkey != undef {
    file { [ "/etc/puppetlabs/puppet/eyaml",
             "/etc/puppetlabs/puppet/eyaml/keys/" ]:
      ensure => directory,
      mode => '755', 
    }
    
  }

  if $privkey != undef {
    file { "/etc/puppetlabs/puppet/eyaml/keys/private_key.pkcs7.pem":
      content => $privkey,
      mode => '640', 
    }
  }
  
  if $pubkey != undef {
    file { "/etc/puppetlabs/puppet/eyaml/keys/public_key.pkcs7.pem":
      content => $pubkey,
      mode => '644', 
    }
  }

  
  
}



# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
