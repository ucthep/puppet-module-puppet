# Class: puppet
#
# Manage puppet agent and master
#
# Parameters:
#  $master: hostname of the Puppet Master
#
#  $r10k_manage: should r10k be managed?
#  $r10k_gitrepo = undef,
#
#  $postgresql_manage = true,
#  $puppetdb_manage = true,
#  $puppetboard_manage = true,
#  $webhook_manage = true,
#
# Actions:
#   - install, configure and run the puppet agent
#   - if the FQDN equals $master, install, configure and run puppetserver
#   - optionally, manage r10k and its webhook, PuppetDB and the backend
#     PostgreSQL DB, PuppetBoard
#
# Requires:
#   - for PuppetBoard: an Apache web server (this should optionally be
#     managed by this module as well)
#
# Sample Usage:
# class { 'puppet':
#     master => 'puppet.example.com',
#     r10k_gitrepo => 'git@bitbucket.org:username/puppet.git'
# }
class puppet (
    $master = undef,
    $master_jvmmem = '-Xms2048m -Xmx2048m',
    
    $r10k_enable = true,
    $r10k_gitrepo = undef,
    #$r10k_extra_sources = {},

    $r10k_sshkey_private = undef,
    $r10k_sshkey_public = undef,
    $r10k_sshdir = "/opt/puppetlabs/server/data/puppetserver/.ssh",
    $r10k_user = 'puppet',
    $r10k_group = 'puppet',
    
    $r10k_webhook_enable = false,
    $r10k_webhook_cert = "/etc/letsencrypt/live/$fqdn/cert.pem",
    $r10k_webhook_key = "/etc/letsencrypt/live/$fqdn/privkey.pem",

    $puppetdb_enable = true,
    
    $puppetboard_enable = true,

    $manage_apache = true,
    $manage_apache_conf = true,
    $manage_firewall = false,

)

{
    # -------------------------------------------------------------------
    # parameter section - this could become a 'params' class
    
    
    # keep a flag if we are running on the puppet master 
    $ismaster = ($master == $fqdn)

    
    info "We are running on $fqdn"

    # we definetly need a puppet.conf
    contain puppet::config

    # we also want to run the agent
    contain puppet::config::ca
    contain puppet::install::agent
    contain puppet::service::agent

    # and now we need to define the order
    Class['puppet::install::agent'] ->
    Class['puppet::config'] ->
    Class['puppet::service::agent']
    
  
    # we install the master only if it points to the current host
    if $ismaster {

        # the actual master
        contain puppet::install::master
        contain puppet::service::master

        if $puppet::r10k_enable {
            contain puppet::r10k
            contain puppet::r10k::sshkeys
            contain puppet::r10k::deploy
        }

        if $puppet::r10k_webhook_enable {
            contain puppet::r10k::webhook
        }
    
        if $puppet::puppetdb_enable {
            contain puppet::extras::puppetdb

            Class['puppet::service::agent'] ->
            Class['puppet::extras::puppetdb']
        }
        
        if $puppet::puppetboard_enable {
            contain puppet::extras::puppetboard

            Class['puppet::extras::puppetdb'] ->
            Class['puppet::extras::puppetboard']
        }

        Class['puppet::install::master'] ->
        Class['puppet::config'] ->
        Class['puppet::r10k'] ->
        Class['puppet::r10k::sshkeys'] ->
        Class['puppet::r10k::deploy'] ->
        Class['puppet::r10k::webhook'] ->
        Class['puppet::service::master'] ->
        Class['puppet::service::agent']


        Class['puppet::config::ca'] ->
        Class['puppet::service::master'] ->
        Class['puppet::service::agent']


    }

  
}



# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
