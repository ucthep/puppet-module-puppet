# Class: puppet::extras
#
# manage extra puppet services (puppetdb, puppetboard,...) through the
# respective modules
#
class puppet::extras
{

    if $puppet::puppetdb_manage {
        include puppet::extras::puppetdb
    }

    if $puppet::puppetboard_manage {
        include puppet::extras::puppetboard
    }

}

# ==========================================================================
class puppet::extras::puppetdb
{
    
    # --------------------------------------------------------------------
    # install and configure a PostgreSQL server and database

    # On Debian 8 (Jessie) a newer version of PgSQL hast to be
    # installed, requiring this gymnastics...

    # define global parameters
    class { 'postgresql::globals':
        version => '9.6',
        manage_package_repo => true,
        #repo_baseurl => 'http://apt.postgresql.org/pub/repos/apt/',
    }

    # install the actual server
    class { 'postgresql::server': }

    # configure a database for puppetdb
    class { 'puppetdb::database::postgresql':
        manage_server => false,
        require => Class['postgresql::server'],
    }

    
    # --------------------------------------------------------------------
    #package { 'puppetdb-termini': ensure => installed }

    # --------------------------------------------------------------------
    # Configure puppetdb, using a PostgreSQL database on localhost
    class { 'puppetdb::server':
        require => Class['puppetdb::database::postgresql'],

        manage_firewall => false,
        ssl_set_cert_paths => true,
        #ssl_cert_path => "/etc/letsencrypt/live/${fqdn}/cert.pem",
        #ssl_key_path => "/etc/letsencrypt/live/${fqdn}/privkey.pem",
        #ssl_ca_cert_path => "/etc/letsencrypt/live/${fqdn}/chain.pem",
        #disable_cleartext => false,
        #disable_ssl => true,
    }

    class { 'puppetdb::master::config':
        #puppetdb_server => 'localhost',
        #puppetdb_port   => 8080,
        #puppetdb_disable_ssl => true,
        manage_storeconfigs => false,
        manage_routes => false,
        
        require => Class['puppet::config::ca'],
        #require => Service['puppet'],
    }
    
}

# ==========================================================================
class puppet::extras::puppetboard
{

    # --------------------------------------------------------------------
    # install python and goodies
    #class { 'python' :
    #    version    => 'system',
    #    pip        => 'present',
    #    dev        => 'present',
    #    virtualenv => 'present',
    #    #gunicorn   => 'absent',
    #}
    include ucthep::sw::python
  
    # make sure we have this before the puppetboard
    Class['python'] -> Class['puppetboard']

    
    # --------------------------------------------------------------------
    # Configure Puppetboard
    class { 'puppetboard': }
    
    # Access Puppetboard from example.com/puppetboard
    if $puppet::manage_apache_conf {
        class { 'puppetboard::apache::conf': }
    }

    # --------------------------------------------------------------------
    # optional: confgigure apache
    if $puppet::manage_apache {
    
        # Configure Apache
        # Ensure it does *not* purge configuration files
        class { 'apache':
            purge_configs => false,
            mpm_module    => 'prefork',
            default_vhost => true,
            default_mods  => false,
        }
        
        class { 'apache::mod::wsgi': }

        # and some hints for the order
        Class['apache']
        -> Class['puppetboard']
        -> Class['puppetboard::apache::conf']

    }
    
}

# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
