# Class: puppet::config
#
# create puppet configuration files
#
# Parameters:
#   $master:
#       puppet master
#
# Actions:
#   - create puppet.conf
#
# Requires:
#   - 
#
# Sample Usage:
#
class puppet::config
{

    File { owner => 'root', group => 'root', mode => '0644' }

    file {
        '/etc/puppetlabs/puppet/puppet.conf':
            content => template('puppet/puppet.conf.erb');

        #'/etc/puppetlabs/puppet/auth.conf':
        #    content => template('puppet/auth.conf.erb'),

            
    }


    if $puppet::ismaster {
        file_line { 'puppetserver JVM args':
            path => '/etc/sysconfig/puppetserver',  
            line => "JAVA_ARGS=\"${puppet::master_jvmmem} -Djruby.logger.class=com.puppetlabs.jruby_utils.jruby.Slf4jLogger\"",
            match   => "^JAVA_ARGS=",
            notify => Class['puppet::service::master'],
        }
    }
    
    if $puppet::ismaster and $puppet::manage_firewall {
        # ----------------------------------------------------------------
        # open the firewall for the puppet master
        firewall { '100: accept puppetserver':
            dport    => 8140,
            proto    => tcp,
            action   => accept,
        }
    }
    
}


class puppet::config::ca
{
    # static flags 
    $ssldir = '/etc/puppetlabs/puppet/ssl'
    $puppet = '/opt/puppetlabs/bin/puppet'
    $test = '/usr/bin/test'
    
    Exec { "puppet cert generate $fqdn":
        command => "$puppet cert generate $fqdn",
        unless => @("EOT"/L)
        $test -f $ssldir/certs/ca.pem \
             -a -f $ssldir/certs/$fqdn.pem \
             -a -f $ssldir/private_keys/$fqdn.pem
        | EOT
    }

}


# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
