# Class: puppet::install
#
# short discription
#
# Parameters:
#   $first_parameter:
#       description
#
# Actions:
#   - 
#
# Requires:
#   - 
#
# Sample Usage:
#
class puppet::install::agent
{
    package { 'puppet-agent': ensure => installed }
}


class puppet::install::master
{
    # This class should only be declared on the master, this if
    # statement is a bit of paranoia.
    if $puppet::ismaster {
        package { 'puppetserver': ensure => installed }
    }
}






# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
