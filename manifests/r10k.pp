class puppet::r10k
{

    $def_sources = {
        'default' => { 
            'remote'  => $puppet::r10k_gitrepo,
            'basedir' => "${::settings::codedir}/environments",
            'prefix'  => false,
        }
    }
    
    class { '::r10k':
        version           => '1.5.1',
        #version           => latest,
        #sources => reduce($puppet::r10k_extra_sources,$def_sources) |$res,$i| {

        sources => reduce({}, $def_sources) |$res,$i| {
            
            # create the value for the hash to be added to the sources
            # list
            $value = {
                'remote'  => $i[1],
                'basedir' => "${::settings::codedir}/environments",
                'prefix'  => true,
            }
            
            # add the key/value pair to the hash
            $tmp = merge($res, {$i[0] => $value} ) 
            
            # return the hash
            $tmp
        },

        notify => Class['puppet::r10k::deploy'],
    }

 
    # -----------------------------------------------------------------
    #
    #   set ownership of directories to be managed by r10k
    #
    # -----------------------------------------------------------------
    File { 
        [ '/etc/puppetlabs/code/environments',
          '/opt/puppetlabs/puppet/cache/r10k' ]:

            ensure => directory,
            recurse => true,
            owner => $puppet::r10k_user,
            group => $puppet::r10k_group,
            before => Class['puppet::r10k::deploy'],
    }

    
}
    #package { 'semantic_puppet':
    #    provider => 'gem',
    #    ensure => '0.1.2',
    #    before => Class['::r10k'],
    #}
    

class puppet::r10k::deploy
{
    info "Running r10k deploy..."
    
    exec { 'r10k deploy':
        command => '/usr/bin/r10k deploy environment -p',
        user => $puppet::r10k_user,
        refreshonly => true,
        subscribe => Class['::r10k::config'],
        require => [ Class['::r10k'] ],
    }
    
}


class puppet::r10k::webhook
{

    # -----------------------------------------------------------------
    #
    #   install a webhook to accept notifications from bitbucket
    #
    # -----------------------------------------------------------------
    class {'r10k::webhook::config':
        use_mcollective => false,

        command_prefix => 'sudo -u puppet',
        client_timeout => 30,
        
        public_key_path   => $puppet::r10k_webhook_cert,
        private_key_path  => $puppet::r10k_webhook_key,

    }

    class {'r10k::webhook':
        user    => 'root', #$puppet::r10k_user,
        group   => 0, #$puppet::r10k_group,
    }
    
    Class['r10k::webhook::config'] -> Class['r10k::webhook']

        

    # --------------------------------------------------------------------
    #
    #   set up the firewall
    #
    # --------------------------------------------------------------------
    if $puppet::manage_firewall {
        firewall { '110 accept puppet-r10k web hook':
            dport  => 8088,
            proto  => tcp,
            action => accept,
        }
    }

}
        
class puppet::r10k::sshkeys
{
    # ------------------------------------------------------------------
    #
    #   Install deploy key for r10k to access bitbucket.org
    #
    # ------------------------------------------------------------------
    # r10k needs read access to the this git repository, to deploy
    # it locally whenever upstream changes are detected.
    #
    # We use the same deploy key on all hosts that need access to
    # the puppet repo on BitBucket. The public and private deploy
    # keys have been added to this repository, but only the
    # private key is needed to access the repo. The public key has
    # been uploaded to BitBucket, and is kept for reference only.
    #
    # The key was generated with the command:
    # ssh-keygen -t rsa -C 'deploy@ucthep' -f site/ucthep/files/puppet/id_deploy
    # ------------------------------------------------------------------

    
    # configure ssh to use special deploy key to connect to bitbucket.org
    ssh::config_entry {
        'deploy bitbucket.org':
            path => "$puppet::r10k_sshdir/config",
            owner => $puppet::r10k_user,
            group => $puppet::r10k_group,
            
            host  => 'bitbucket.org',
            lines => ["  IdentityFile $puppet::r10k_sshdir/id_deploy"],
            notify => Exec['ssh git@bitbucket.org'],
    }
    
    # default file permissions
    File { 
        owner => $puppet::r10k_user,
        group => $puppet::r10k_group,
    }

    file {
        $puppet::r10k_sshdir:
            ensure => directory,
            before => File[ 'id_deploy', 'id_deploy.pub'];
        
        
        # install the private/public keypair
        'id_deploy':
            path => "$puppet::r10k_sshdir/id_deploy",
            source => $puppet::r10k_sshkey_private,
            mode => '0400';

        'id_deploy.pub':
            path => "$puppet::r10k_sshdir/id_deploy.pub",
            source => $puppet::r10k_sshkey_public,
            mode => '0644';
    }
    
    # make sure the host key of bitbucket.org gets added to the known hosts 
    exec { 'ssh git@bitbucket.org':
        command => '/usr/bin/ssh -o StrictHostKeyChecking=no git@bitbucket.org',
        user => $puppet::r10k_user,
        refreshonly => true,
        require => [ Ssh::Config_entry['deploy bitbucket.org'],
                     File[ 'id_deploy', 'id_deploy.pub'] ]
    }
    
    # ------------------------------------------------------------------
    # This command can be used to create the key on the fly:
    #exec { 'create deploy sshkey':
    #    command => "/usr/bin/ssh-keygen -t rsa -C deploy@$fqdn -f /root/.ssh/id_deploy",
    #    creates => '/root/.ssh/id_deploy',
    #    before => Exec['ssh git@bitbucket.org'],
    #}

}

# Local Variables:
#   mode: puppet
#     puppet-indent-level: 4
#     indent-tabs-mode: nil
# End:
