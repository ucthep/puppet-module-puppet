# puppet

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with puppet](#setup)
    * [What puppet affects](#what-puppet-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with puppet](#beginning-with-puppet)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

Configure Puppet for a small site with a single, full-featured Puppet
Master with r10k and PuppetBoard.

## Module Description

The module handles the setup of a site with a single puppet master and
the connected agents. It assumes that a single server runs the
puppetserver, PuppetDB (and the PostgreSQL DB) and PuppetBoard (in
Apache). The site configuration is kept in an external (BitBucket) git
repository that is deployed on the puppet master with r10k. Changes
pushed to BitBucket will can deployed to the puppet master through a
webhook. Mcollective is not supported, and hiera is not used in this
module.

The module aims at making the setup of such a site as simple as
possible. Ideally, only very few settings have to be adjusted, such as
the hostname of the puppet master and the URL of the control
repository.

For most of the services, other puppet modules are used for all the
heavy-lifting, and this modules acst mainly as a wrapper for them.


## Setup

### What puppet affects

The module can control all aspects of the Puppet infrastructure for a
small site. All components except for the agent can be enabled or
disabled.

* Configuration and activation of the puppet agent
* Configuration and activation of a puppet master
* Installation and configuration of r10k
* Installation of a webhook for r10k
* Installation and setup of a PuppetDB
* Installation and setup of PuppetBoard

### Setup Requirements

Puppet must be installed on all nodes where this module is to be
used. This module will not upgrade Puppet, so this should
be the version that is to be used in the final setup.

The external git control repository must be created before the master
is deployed. At the very least, it should contain a `Puppetfile` to
reference this and additional modules and a `manifests/site.pp` that
contains either a default node definition or one specific to the
puppet master and all the clients to be installed.

Access to BitBucket will have to be configured separately. 


## Usage

### Puppet installation

You will have to install your preferred version of puppet, either
through the packages provided by your operating system, or using one
of the official puppet platforms.

You will have to make this module available. At some point, it might
end up on the Puppet Forge, until then you will have to either clone
the repository into a module directory, or build the module and
install it by hand.

Note that cloning the repository into a module directory that is
accessible to puppet will not install any dependencies - this will
have to be done by hand; you fill find a list of (hopefully up to
date) dependencies in the `metadata.json` file.

Alternatively, you can check out a copy of the module and build it
locally with
```
puppet module build
```

Afterwards, you can install the module with the following command,
that should pull in any additional dependencies (if `metadata.json` is
up to date). 
```
puppet module install ucthep-puppet-X.Y.Z.tar.gz
```

I leave it to the reader as a simple exercise to figure out in which
directories to run these commands ;-)

### Git repository setup

You will then have to prepare a control repository, put it on a git
hosting provider, and secure it.

At the very least, the repository should contain a `Puppetfile` and a
site manifest (`manifests/site.pp`) or several host manifests. The
`Puppetfile` lists all dependencies, including this module. Note that
r10k does not support dependency resolution, so you will also have to
list the dependencies of the depencies. The site manifest will control
the configuration of your nodes.

You should generate a **deploy key** that will have read access to the
repository and upload it to the repository. Refer to the documentation
of your git hosting provider for this.

To facilitate automatic deployment, you may want to set up a
**webhook** that will notify your puppetmaster of pushes to the
control repository. Set the URL to
`https://puppet:puppet@puppet.example.com:8088/payload`. Note that you
can also notify the master of changes to your modules if you setup a
hook pointing to `.../modules`.



### Initial setup

Once the puppet, this module and the control repository have been set
up, you can install the puppet master by running this manifest:
```
class {"puppet": 
	master => "puppet.example.com",
    r10k_gitrepo => "git@bitbucket.org:joeuser/puppet.git"
}
```


### Installing clients

The addition of new clients is simple: install puppet and point the
agent to your puppet master:
```
puppet agent --test --server puppet.example.com --waitforcert 60
```

For each new client, you will have to log into the master and sign the
client certificate:
```
puppet cert list
puppet sign newclient.example.com
```

## Reference

For now, this is just a collection of personal remarks, that should
remind me of the most basic implementation details.

### Top-level class "puppet"

Everything should be configured through a single top-level class
`puppet`, which is used on the master as well as the clients. This
class with configure an agent and - if the `puppetmaster` argument
matches the hostname - a full-featured puppetmaster. 

The reason for using a single top-level class is that the
configuration file is shared between the agent and the master, and is
therefore be generated by a single class `puppet::config`.

The top-level class calls a number of other classes to do the actual
work, or delegate the work to yet another module. 


### r10k and webhook

The module will use r10k for automatic deployment and environment
management, relying on a single control repository that is hosted
externally, e.g. on BitBucket. 

Because the control repository may contain sensitive information
(e.g. passwords, site configuration with possible weaknesses) , it
should be protected. The control repo is accessed with a special
"deploy" ssh key, that is kept in the control repository and that will
be installed by this module. For ssh, it seems to be necessary to
install both the public and private key, therefore both must be given
as arguments to the `puppet` class. 

r10k runs as the user "puppet" (set by the r10k_user option to the
puppet class), and therefore the ssh access has to be configured for
that user. The user must have write access to the staging area for
environments (/etc/puppetlabs/code/environments), and the cache for
git repositories. This module changes ownership of these directory
trees to the puppet user.



## Limitations

I am not a puppet expert, so the module might not be the most elegant,
most configurable or most portable, but it gets the job done for me. 

Feel free to use it, improve it. 

## Development

Ask the UCT HEP guys for more info.


